var Informer = (function () {
    "use strict";

    var elem = $('<div class="_informer" style="display: none;"><span></span></div>'),
        hideHandler,
        that = {};

    var show = function (text) {
        clearTimeout(hideHandler);

        elem.find("span").html(text);
        elem.delay(200).fadeIn().delay(4000).fadeOut();
    };

    that.text = function (text) {
        elem.css('border', '1px solid #BCE8F5').css('color', 'white');
        show(text);
    };

    that.success = function (text) {
        elem.css('color', '#00FF77').css('border', '1px solid #BCF5E8');
        show(text);
    };

    that.error = function (text) {
        elem.css('border', '1px solid #F5BBBB').css('color', '#FF5555');
        show(text);
    };

    $(document).ready(function () {
        $('body').append(elem);
    });
    return that;
}());


var editableTable = (function () {
    "use strict";

    var that = {},
        root_elem, modal,
        ajaxHandler,
        fields, table, tbody,
        buttonAdd, buttonEdit, buttonRemove,
        data;

    that.init = function (options) {
        root_elem   = $(options.selector);
        fields = options.fields;
        ajaxHandler = options.ajaxHandler;

        root_elem.html('').addClass('_editable');
        var control_buttons = $('<div class="control_buttons btn-group"   role="group"></div>');
        buttonAdd = $('<button type="button" class="btn btn-default" data-action="add">Add</button>');
        buttonEdit = $('<button type="button" class="btn btn-default" data-action="edit" disabled>Edit</button>');
        buttonRemove = $('<button type="button" class="btn btn-default" data-action="remove" disabled>Delete</button>');
        control_buttons.append(buttonAdd, buttonEdit, buttonRemove);
        root_elem.prepend(control_buttons);

        table = $('<table class="table table-hover table-bordered"></table>');
        root_elem.append(table);


        var thead = $('<thead></thead>');
        table.prepend(thead);
        tbody = $('<tbody></tbody>');
        table.append(tbody);
        var tfoot = $('<tfoot></tfoot>');
        table.append(tfoot);

        modal = $('<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true"></div>');
        var modalContent = $('<div class="modal-content"></div>');
        modal.append(modalContent);

        var modalHeader = $(
            '<div class="modal-header">' +
                '<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>'+
                '<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
                '<span aria-hidden="true">&times;</span>'+
                '</button>'+
            '</div>'
        );

        var modalBody = $('<div class="modal-body">' +
            '<div class="fieldset hidden">' +
            '<label for="id"></label>' +
            '<input name="id"/>' +
            '</div>' +
            '</div>');
        var modalFooter = $(
            '<div class="modal-footer">'+
                '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>'+
                '<button type="button" class="btn btn-primary"></button>'+
            '</div>'
        );

        modalContent.append(modalHeader, modalBody, modalFooter);

        var labelsRow = $('<tr><th data-name="id" class="hidden">ID</th></tr>');
        $(fields).each(function () {
            var th = $('<th data-name="' + this.name + '">' + this.label + '</th>');
            labelsRow.append(th);

            if (this.renderer == undefined){
                var formFieldset = $('<div class="fieldset"></div>');
                var formLabel    = $('<label for="' + this.name + '">' + this.label + '</label>');
                var formInput    = $('<input name="' + this.name + '"/>');
                formFieldset.append(formLabel, formInput);
                modalBody.append(formFieldset);
            }

        });
        thead.append(labelsRow.clone());
        tfoot.append(labelsRow.clone());

        root_elem.append(modal);




        table.on('click', 'tr[data-rowid]', function () {
            if ( !$(this).hasClass('bg-info')){
                $(this).addClass('bg-info');//.siblings().removeClass('bg-info');
            } else {
                $(this).removeClass('bg-info');
            }
            var selectedCount = table.find('tr.bg-info').length;
            if (selectedCount === 0 ){
                buttonRemove.prop('disabled', true);
                buttonEdit.prop('disabled', true);
            } else if ( selectedCount === 1 ){
                buttonRemove.prop('disabled', false);
                buttonEdit.prop('disabled', false);
            } else {
                buttonRemove.prop('disabled', false);
                buttonEdit.prop('disabled', true);
            }
        });

        root_elem.on('click', '.control_buttons button', function () {
            showModal($(this).attr('data-action'), table.find('tr.bg-info'))
        });

        root_elem.on('click', '.btn-primary', function () {
            ajaxEvent();
        })
    };

    var showModal = function (event, object) {
        modal.attr('data-type', event);
        switch (event) {
            case 'add':
                modal.find('.modal-title').html('New Entry');
                modal.find('.modal-body').find('input').val('');
                break;
            case 'edit':
                modal.find('.modal-title').html('Edit Entry');
                object.find('[data-name]').each(function () {
                    var name = $(this).attr('data-name');
                    var value = $(this).text().trim();
                    modal.find('.modal-body').find('input[name="' + name + '"]').val(value);
                });
                break;
            case 'remove':
                var ids = $.map(object, function (a) {
                    return $(a).attr('data-rowid');
                });
                modal.find('input[name=id]').val(ids.join(','));
                modal.find('.modal-title').html('Confirm Delete '+ object.length +((object.length === 1)?' entry':' entries'));
                break;
        }
        modal.modal();
    };

    var ajaxEvent = function () {
        var data = {};
        $('.modal input').each(function (a, b) {
            data[$(b).prop('name')] = $(b).val();
        });
        $.ajax({
            url      : ajaxHandler,
            type     : 'post',
            dataType : 'json',
            data     : {action : modal.attr('data-type'), data: data}
        }).done(function (msg) {
            if (msg.status == true){
                switch (modal.attr('data-type')) {
                    case 'add':
                        if (msg.data !== undefined){
                            updateRow(msg.data);
                        }
                        break;
                    case 'edit':
                        updateRow(data);
                        break;
                    case 'remove':
                        tbody.find('tr[data-rowid]').each(function () {
                           if ($.inArray($(this).attr('data-rowid'), data.id.split(',')) !== -1){
                               $(this).remove();
                           }
                        });

                        break;
                }
                Informer.success('success');
                $('.modal').modal('hide');
            }
            if (msg.error !== undefined){
                $('.modal').modal('hide')
                Informer.error(msg.error);
            }
        });
    };

    that.load = function () {
        $.ajax({
            url: ajaxHandler,
            type: 'post',
            dataType: 'json',
            data: {action: 'load'}
        }).done(function (msg) {
            var realFieldsCount = 0;
            $.each(fields, function () {
                if (this.renderer == undefined) realFieldsCount++
            });
            var error;
            if (msg.data){
                var data = msg.data;
                $.each(data, function () {
                    if (Object.keys(this).length !== realFieldsCount+1) error = 'Wrong json fields count';
                    if ($.inArray('id', Object.keys(this)) === -1) error = 'Wrong json format';
                });
                if (error !== undefined){
                    Informer.error(error);
                } else {
                    $.each(data, function () {updateRow(this)});
                }
            } else {
                Informer.error('Wrong ajax answer');
            }
        }).fail(function () {
            Informer.error('Network Error');
        });
    };

    var updateRow = function (row) {
        var completeRow = jQuery.extend(true, {id:{label : "ID", name : "id"}}, fields);
        $.each(completeRow, function () {
            if (this.renderer !== undefined){
                this.value = this.renderer(row);
            } else {
                this.value = row[this.name]
            }
        });

        var updated = false;
        tbody.find('[data-rowid]').each(function () {
            if (completeRow.id.value == $(this).attr('data-rowid')){
                var tr = $('<tr data-rowid = "' + completeRow.id.value + '"></tr>');
                tr.append('<td data-name="id" class="hidden">'+ completeRow.id.value+'</td>');
                $.each(completeRow, function (key, value) {
                    if (key !== 'id') {
                        var td = $('<td data-name="' + this.name + '">' + this.value + '</td>');
                        tr.append(td);
                    }

                });
                $(this).html(tr.html());
                updated = true;
            }
        });
        if (!updated) { //todo add
            var tr = $('<tr data-rowid = "'+ completeRow.id.value+'"></tr>');
            tr.append('<td data-name="id" class="hidden">' + completeRow.id.value + '</td>');
            $.each(completeRow, function (key, value) {
                if (key !== 'id'){
                    var td = $('<td data-name="' + this.name + '">' + this.value + '</td>');
                    tr.append(td);
                }

            });
            tbody.append(tr);
        }
    };

    return that;
}());

<?php

$conformity = include_once 'conformity.php';

if (!extension_loaded('pinba')) {
    if (!function_exists('pinba_timer_start')) {
        function pinba_timer_start($a) { }
    }
    if (!function_exists('pinba_script_name_set')) {
        function pinba_script_name_set($a) { }
    }
    if (!function_exists('pinba_timer_stop')) {
        function pinba_timer_stop($a) { }
    }
    if (!function_exists('pinba_flush')) {
        function pinba_flush($a) { }
    }
}

require_once '/var/www/vhosts/common/vendor/autoload.php';
$useragent = $_SERVER['HTTP_USER_AGENT'];

function getOs(){
    $useragent = $_SERVER['HTTP_USER_AGENT'];
    if (preg_match('/windows|win32/i', $useragent)) {
        $os_platform = 'win';
    } else if (preg_match('/macintosh|mac os x/i', $useragent)) {
        $os_platform = 'mac';
    } else if (preg_match('/linux/i', $useragent)) {
        $os_platform = "lin";
    } else {
        $os_platform = "unk";
    }
    return $os_platform;
}

function getBrowser(){
    $useragent = $_SERVER['HTTP_USER_AGENT'];
    $browser = 'empty';
    if (preg_match('/(OPR|Opera|Chrome|Safari|Firefox|MSIE)/i', strtolower($useragent), $match)) {
        if ($match[1] == 'chrome' && preg_match('/OPR/i', strtolower($useragent), $match_opera)) {
            $browser = 'opera';
        } else {
            $browser = ($match[1] == 'msie') ? 'ie' : $match[1];
        }
        if (!isset($browser)) $browser = 'empty';
    }
    return $browser;
}

function getIp(){
    $ip = (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
    if (isset($_SERVER['HTTP_CLIENT_IP'])) $ip = $_SERVER['HTTP_CLIENT_IP'];
    return $ip;
}

function getCountry(){
    $ip = getIp();
    $country = (@geoip_country_code_by_name($ip)) ? @geoip_country_code_by_name($ip) : 'un';
    return $country;
}

function pushPinba(){
    $country_os_browser = getCountry() . '/' . getOs() . '/' . getBrowser();
    pinba_timer_start(array('server' => $_SERVER['SERVER_NAME'], 'country' => $country_os_browser));
    pinba_script_name_set('index.php');

}

function toCounter($id)
{
    $data[] = array
    (
        'time' => time(),
        'counter_id' => $id,
        'value' => 1,
    );

    $ch = curl_init('http://goldbar.86400.ru/receiver.php');

    curl_setopt_array($ch, array
    (
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_POSTFIELDS => array('data' => json_encode($data)),
    ));

    curl_exec($ch);
    curl_close($ch);
}

function toLog($logfile){
    file_put_contents(
        __DIR__ . '/'. $logfile,
        json_encode(
            array_merge(
                $_GET,
                array(
                    'ts' => time(),
                    'ip' => getIp(),
                    'country' => getCountry(),
                    'browser' => getBrowser(),
                    'os' => getOs()
                )
            )
        ) . ",\n", FILE_APPEND);
}

if (getIp() == '23.21.124.216'){
    echo 'Ok';
    exit();
}

pushPinba();
$cids = array();
foreach ($conformity as $item) {
    $cids[] = $item['cid'];
}
if ( array_key_exists('camp_id',$_GET) && in_array(strtolower($_GET['camp_id']), $cids) ) {
    toLog('loger.json');
    $location = '';
    foreach ($conformity as $item) {
        if (strtolower($item['cid']) == strtolower($_GET['camp_id'])) {
            $location = $item['offer_url'];
        }
    }
    if (array_key_exists('param',$_GET)) $location .= '&s2=' . $_GET['param'];
    if (array_key_exists('gclid',$_GET)) $location .= '&s3=' . $_GET['gclid'];
    header('Location: '. $location);
} else {
    toLog('errors.json');
    toCounter(1919356747);
}

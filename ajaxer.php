<?php

$dataFileName = 'conformity.php';

function saveArray($array, $filename){
    $backupfile = '';
    foreach (explode('.', $filename) as $key => $item) {
        if ($key+1 < count(explode('.', $filename))){
            $backupfile .= $item;
        } else {
            $backupfile = substr($backupfile, 0, strlen($backupfile)) . '_' . ((string)time()) . '.' . $item;
        }
    }

    exec('cp ' . $filename . ' ' . $backupfile);
    $text = "<?php\n\nreturn ".var_export($array, true).';';
    return file_put_contents($filename, $text);
}


if (array_key_exists('action', $_POST)){
    if (array_key_exists('data', $_POST)) {$data = $_POST['data'];}
    $answer = array();
    $current_data = include_once $dataFileName;
    switch ($_POST['action']){
        case 'load':
            $answer['data'] = $current_data;
            break;
        case 'add':
            if (isset($data)) {
                $newRow = $data;
                $array_column = array();
                foreach ($current_data as $item) {
                    $array_column[] = $item['id'];
                }
                $newRow['id'] = max($array_column) + 1;
                if (saveArray(array_merge($current_data, array($newRow)), $dataFileName) !== false) {
                    $answer['data'] = $newRow;
                    $answer['status'] = true;
                } else {
                    $answer['error'] = 'write data error';
                }
            } else {
                $answer['error'] = 'now data to set';
            }
            break;
        case 'edit':
            if (isset($data)){
                foreach ($current_data as $key => $row) {
                    if ($row['id'] == $data['id']){
                        $current_data[$key] = $data;
                    }
                }
                if (saveArray($current_data, $dataFileName) !== false){
                    $answer['status'] = true;
                } else {
                    $answer['error'] = 'write data error';
                }
            } else {
                $answer['error'] = 'now data to set';
            }
            break;
        case 'remove':
            if (isset($data)) {
                $ids2Delete = explode(',',$data['id']);
                foreach ($current_data as $key => $row) {
                    if (in_array($row['id'], $ids2Delete)) {
                        unset($current_data[$key]);
                    }
                }
                if (saveArray($current_data, $dataFileName) !== false) {
                    $answer['status'] = true;
                } else {
                    $answer['error'] = 'write data error';
                }
            } else {
                $answer['error'] = 'now data to set';
            }
            break;
        default :
            $answer['error'] = 'unknown action';
            break;
    }

    echo json_encode($answer);
}
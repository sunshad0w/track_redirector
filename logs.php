<?php

function last_lines($path, $line_count, $block_size = 512)
{
    $lines = array();

    // we will always have a fragment of a non-complete line
    // keep this in here till we have our next entire line.
    $leftover = "";

    $fh = fopen($path, 'r');
    // go to the end of the file
    fseek($fh, 0, SEEK_END);
    do {
        // need to know whether we can actually go back
        // $block_size bytes
        $can_read = $block_size;
        if (ftell($fh) < $block_size) {
            $can_read = ftell($fh);
        }

        // go back as many bytes as we can
        // read them to $data and then move the file pointer
        // back to where we were.
        fseek($fh, -$can_read, SEEK_CUR);
        $data = fread($fh, $can_read);
        $data .= $leftover;
        fseek($fh, -$can_read, SEEK_CUR);

        // split lines by \n. Then reverse them,
        // now the last line is most likely not a complete
        // line which is why we do not directly add it, but
        // append it to the data read the next time.
        $split_data = array_reverse(explode("\n", $data));
        $new_lines  = array_slice($split_data, 0, -1);
        $lines      = array_merge($lines, $new_lines);
        $leftover   = $split_data[count($split_data) - 1];
    } while (count($lines) < $line_count && ftell($fh) != 0);
    if (ftell($fh) == 0) {
        $lines[] = $leftover;
    }
    fclose($fh);
    // Usually, we will read too many lines, correct that here.
    return array_slice($lines, 0, $line_count);
}

$errors = json_decode("[" . substr(join("\n", last_lines(__DIR__ . '/errors.json', 51)), 0, -1) . "]", true);
$logs = json_decode("[" . substr(join("\n", last_lines(__DIR__ . '/loger.json', 51)), 0, -1) . "]", true);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Logs</title>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        th, tr {
            padding: 0 10px;
        }

        td {
            text-align: center;
        }

        .red{
            color: red;
        }
    </style>

</head>
<body>
<h3>
    <a href="errors.json">errors</a>
</h3>
<table class="table" id="errors">
    <thead>
    <tr>
        <th>camp_id</th>
        <th>param</th>
        <th>gclid</th>
        <th>timestamp</th>
        <th>ip</th>
        <th>country</th>
        <th>browser</th>
        <th>os</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($errors as $line) {
        $camp_id   = (array_key_exists('camp_id', $line)) ? $line['camp_id'] : '<span class="red">null</span>';
        $param     = (array_key_exists('param', $line)) ? $line['param'] : '<span class="red">null</span>';
        $gclid     = (array_key_exists('gclid', $line)) ? $line['gclid'] : '<span class="red">null</span>';
        $timestamp = (array_key_exists('ts', $line)) ? $line['ts'] : '<span class="red">null</span>';
        $ip        = (array_key_exists('ip', $line)) ? $line['ip'] : '<span class="red">null</span>';
        $country   = (array_key_exists('country', $line)) ? $line['country'] : '<span class="red">null</span>';
        $browser   = (array_key_exists('browser', $line)) ? $line['browser'] : '<span class="red">null</span>';
        $os        = (array_key_exists('os', $line)) ? $line['os'] : '<span class="red"><span class="red">null</span></span>';
        ?>
        <tr>
            <td><?= $camp_id ?></td>
            <td><?= $param ?></td>
            <td><?= $gclid ?></td>
            <td class="ts"><?= $timestamp ?></td>
            <td><?= $ip ?></td>
            <td><?= $country ?></td>
            <td><?= $browser ?></td>
            <td><?= $os ?></td>
        </tr>
        <?php
    }
    ?>
    </tbody>
</table>

<h3>
    <a href="loger.json">ok log</a>
</h3>
<table class="table" id="loger">
    <thead>
    <tr>
        <th>camp_id</th>
        <th>param</th>
        <th>gclid</th>
        <th>timestamp</th>
        <th>ip</th>
        <th>country</th>
        <th>browser</th>
        <th>os</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($logs as $line) {
        $camp_id   = (array_key_exists('camp_id', $line)) ? $line['camp_id'] : '<span class="red">null</span>';
        $param     = (array_key_exists('param', $line)) ? $line['param'] : '<span class="red">null</span>';
        $gclid     = (array_key_exists('gclid', $line)) ? $line['gclid'] : '<span class="red">null</span>';
        $timestamp = (array_key_exists('ts', $line)) ? $line['ts'] : '<span class="red">null</span>';
        $ip        = (array_key_exists('ip', $line)) ? $line['ip'] : '<span class="red">null</span>';
        $country   = (array_key_exists('country', $line)) ? $line['country'] : '<span class="red">null</span>';
        $browser   = (array_key_exists('browser', $line)) ? $line['browser'] : '<span class="red">null</span>';
        $os        = (array_key_exists('os', $line)) ? $line['os'] : '<span class="red"><span class="red">null</span></span>';
        ?>
        <tr>
            <td><?= $camp_id ?></td>
            <td><?= $param ?></td>
            <td><?= $gclid ?></td>
            <td class="ts"><?= $timestamp ?></td>
            <td><?= $ip ?></td>
            <td><?= $country ?></td>
            <td><?= $browser ?></td>
            <td><?= $os ?></td>
        </tr>
        <?php
    }
    ?>
    </tbody>
</table>
</body>
<script>
    $('.ts').each(function () {
        if ($(this).text() !== 'null'){
            $(this).attr('title', new Date(parseInt($(this).text())*1000));
        }
    })
</script>
</html>

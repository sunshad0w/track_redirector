<?php
if (!function_exists('basic_auth')) {
    function basic_auth($users, $message = 'Restricted Area')
    {
        if (isset($_SERVER['PHP_AUTH_USER']) and isset($_SERVER['PHP_AUTH_PW'])) {
            $user = strtolower($_SERVER['PHP_AUTH_USER']);

            if (isset($users[$user]) and $users[$user] === md5($_SERVER['PHP_AUTH_PW'])) {
                return true;
            }
        }

        header('WWW-Authenticate: Basic realm="' . $message . '"');
        header('HTTP/1.0 401 Unauthorized');
        exit;
    }
}

$auth = basic_auth(array('admin' => md5('nimda2017')));
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
    <script type="application/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>
    <script type="application/javascript" src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
    <script type="application/javascript" src="editable.js"></script>
    <link rel="stylesheet" href="editable.css">


    <style>
        #manager {
            max-width: 1000px;
            width: 80vw;
            margin:50px auto 0;
        }
    </style>
</head>
<body>
<div id="manager"></div>

</body>
<script>

    var rendererF = function (itemRow) {
        return location.origin + '/?camp_id='+itemRow.cid+'&amp;param=xxx';
    };

    var fields = [
        {
            label : "Company ID",
            name  : "cid"
        }, {
            label : "Offer Url",
            name  : "offer_url"
        }, {
            label : "AdWords Url",
            name  : "adw_url",
            renderer : rendererF
        }
    ];

    $(document).ready(function () {
        editableTable.init({
            "selector" : "#manager",
            "fields"   : fields,
            "ajaxHandler" : 'ajaxer.php'
        });


        editableTable.load();
    });

</script>
</html>
